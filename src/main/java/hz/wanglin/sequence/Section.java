package hz.wanglin.sequence;

import hz.wanglin.sequence.source.StringSource;

import java.text.Format;
import java.util.function.Supplier;

/**
 * 序列化生成器的格式段{Section1}{Section2}{Section3}......
 */
public class Section {
    /**
     * 格式化表达式
     */
    Format           format;
    /**
     * 序列号生成器
     */
    Supplier<Source> sourceSupplier;
    /**
     *
     */
    Source.Config    sc;


    public Section(Source source, Format format) {
        assert source != null : "SequenceSource is null";
        this.sourceSupplier = new Supplier<Source>() {
            @Override
            public Source get() {
                return source;
            }
        };
        this.format = format;
    }


    public Section(Supplier<Source> sourceSupplier, Format format, Source.Config sc) {
        assert sourceSupplier != null : "SequenceSource is null";
        this.sourceSupplier = sourceSupplier;
        this.format = format;
        this.sc = sc;
    }


    public String value() {
        return format.format(sourceSupplier.get().increBy(sc));
    }

    public Source source() {
        return sourceSupplier.get();
    }

}